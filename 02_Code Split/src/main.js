console.log("hello main");

document.getElementById("btn").onclick = function () {
  // eslint会对动态导入语法报错，需要修改eslint配置文件
  // webpackChunkName: "math"：这是webpack动态导入模块命名的方式
  // "math"将来就会作为[name]的值显示
  import("./math.js").then(({ sum }) => {
    // then 模块加载成功
    alert(sum(1, 2, 3, 4, 5));
  }).catch(() => {
    // catch 模块加载失败
    console.log("模块加载失败")
  });
};