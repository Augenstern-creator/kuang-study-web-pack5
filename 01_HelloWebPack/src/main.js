import count from "./js/count";
import sum from "./js/sum";
// 引入 Css 资源，Webpack才会对其打包
import "./css/index.css";
// 引入 Less 资源，Webpack才会对其打包
import "./less/index.less";
import "./sass/index.sass";
import "./sass/index.scss";
import "./styl/index.styl";
import "./css/iconfont.css";

console.log(count(2, 1));
console.log(sum(1, 2, 3, 4));


// 添加promise代码
const promise = Promise.resolve();
promise.then(() => {
  console.log("hello promise");
});


// 离线访问
if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    navigator.serviceWorker
        .register("/service-worker.js")
        .then((registration) => {
          console.log("SW registered: ", registration);
        })
        .catch((registrationError) => {
          console.log("SW registration failed: ", registrationError);
        });
  });
}