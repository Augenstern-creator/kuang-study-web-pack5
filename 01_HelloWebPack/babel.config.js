module.exports = {
  // 智能预设：能够编译ES6语法
  presets: [
    [
      "@babel/preset-env",
      // 按需加载core-js的polyfill

      {
        // 按需加载自动引入
        useBuiltIns: "usage",
        corejs: {
          //corejs 版本为3
          version: "3", proposals: true
        }
      },
    ],
  ],
};